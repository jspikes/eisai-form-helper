from flask import Flask
from flask_restx import Api, Resource, fields, reqparse
from log.logging import Logger
from PyPDF2 import PdfFileReader
from werkzeug.datastructures import FileStorage
import os

flask_app = Flask(__name__)
api = Api(app = flask_app,
                version = "0.1",
                title = "PDF Parser",
                description = "Helper tool for parsing Eisai 1345 forms")
namespace = api.namespace('PDF', description='PDF Operations')

pdf = api.model('PDF', {
    'id': fields.Integer(readOnly=True, description='ID'),
    'example': fields.String(required=True, description='Example')
})
upload_parser = api.parser()
upload_parser.add_argument('file', location='files',
                           type=FileStorage, required=True)


@namespace.route('/')
class PDF(Resource):
    @namespace.doc('PDF Upload')
    @api.expect(upload_parser)
    def post(self):
        args = upload_parser.parse_args()
        file = args['file']
        file_info = Parse()
        file_info.parse(file)
        return str(file_info.info)     

class Parse():
    info = ''
    def parse(self, file):
        file.save('file.pdf')
        with open('./file.pdf', 'rb') as file_obj:
            pdf = PdfFileReader(file_obj)
            #https://github.com/mstamy2/PyPDF2/issues/51
            if pdf.isEncrypted:
                pdf.decrypt('')
            information = pdf.getDocumentInfo()
            self.info = pdf.getFields()

if __name__ == '__main__':
    flask_app.run(debug=True)