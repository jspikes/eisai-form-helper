import logging
import colorlog 

class Logger():
    logging.basicConfig(
        level = logging.INFO,
        handlers=[
            logging.FileHandler("{0}/{1}.log".format("./", "pdfparser"))
    ])
    logger = logging.getLogger()
    #https://github.com/siemens/kas/blob/master/kas/kas.py
    format_str = '%(asctime)s - %(levelname)s - %(message)s'
    date_format = '%Y-%m-%d %H:%M:%S'
    cformat = '%(log_color)s' + format_str
    colors = {'DEBUG': 'reset',
                'INFO': 'reset',
                'WARNING': 'bold_yellow',
                'ERROR': 'bold_red',
                'CRITICAL': 'bold_red'}
    formatter = colorlog.ColoredFormatter(cformat, date_format,
                                            log_colors=colors)
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(formatter)
    logger.addHandler(stream_handler)